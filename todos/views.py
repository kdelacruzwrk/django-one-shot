from django.shortcuts import get_object_or_404, redirect, render
import todos
from todos.forms import TodoListForm
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todolist_object": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)


    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            list = form.save()

            return redirect("todo_list_detail", id=list.id)
    else:

        form = TodoListForm(instance=todo_list)



    context = {
        "todolist_object": todo_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)




def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
