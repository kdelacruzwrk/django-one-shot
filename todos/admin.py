from django.contrib import admin
from todos.models import TodoItem, TodoList

# Register your models here.
@admin.register(TodoList)
class ToDoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "list",
    ]
