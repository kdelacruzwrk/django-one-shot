from django.forms import ModelForm
from todos.models import TodoList


class TodoListForm(ModelForm):    # Step 1
    class Meta:                 # Step 2
        model = TodoList          # Step 3
        fields = [              # Step 4
            "name",            # Step 4
        ]
